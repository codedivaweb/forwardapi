var express = require('express');
var fs = require('fs');
var path = require('path');
var http = require('http');
var https = require('https');
var app = express();

var request = require('request');
const bodyParser = require('body-parser');

var HTTP_PORT = 8080;

// Create an HTTP service
http.createServer(app).listen(HTTP_PORT,function() {
  console.log('Listening HTTP on port ' + HTTP_PORT);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

/*app.all('get', function(req, res) {

  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  console.log("URL:"+req.originalUrl);
  console.log("METHOD:"+req.method);
  console.log("HEADERS:"+ req.headers);
  console.log("HEADERS RAW:"+ JSON.stringify(req.headers));
  console.log("HEADERS access_token:"+ req.headers.access_token);
  console.log("BODY:"+ req.body);
  console.log("BODY RAW:"+ JSON.stringify(req.body));

  var uri = 'https://httpbin.org';
  request({
      uri: uri + req.originalUrl,
      //headers: req.headers,
      headers: {
        'device_type': req.headers.device_type,
        'device_id': req.headers.device_id,
        'app_id': req.headers.app_id,
        'Authorization': req.headers.Authorization,
        'access_token': req.headers.access_token,
        'refresh_token': req.headers.refresh_token
      },
      body: JSON.stringify(req.body)
      }).pipe(res);
});
*/
app.get('*', function(req, res) {

  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  console.log("URL:"+req.originalUrl);
  console.log("METHOD:"+req.method);
  console.log("HEADERS:"+ req.headers);
  console.log("HEADERS RAW:"+ JSON.stringify(req.headers));
  console.log("HEADERS access_token:"+ req.headers['access_token']);
  console.log("HEADERS accept-encoding:"+ req.headers['accept-encoding']);
  console.log("HEADERS content-type:"+ req.headers['content-type']);
  console.log("HEADERS accept:"+ req.headers['accept']);
  console.log("HEADERS Authorization:"+ req.headers['Authorization']);
  
  console.log("BODY:"+ req.body);
  console.log("BODY RAW:"+ JSON.stringify(req.body));


  var uri = 'http://161.82.176.94';
  request({
      uri: uri + req.originalUrl,
      //headers: req.headers,
      method: req.method,
      headers: {
        'accept-encoding': req.headers['accept-encoding'],
        'content-type': req.headers['content-type'],
        'accept': req.headers['accept'],
        'device_type': req.headers['device_type'],
        'device_id': req.headers['device_id'],
        'app_id': req.headers['app_id'],
        'Authorization': "Bearer " + req.headers['access_token']
      }
      }).pipe(res);
      
});

app.post('*', function(req, res) {

  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  console.log("URL:"+req.originalUrl);
  console.log("METHOD:"+req.method);
  console.log("HEADERS:"+ req.headers);
  console.log("HEADERS RAW:"+ JSON.stringify(req.headers));
  console.log("HEADERS access_token:"+ req.headers['access_token']);
  console.log("HEADERS accept-encoding:"+ req.headers['accept-encoding']);
  console.log("HEADERS content-type:"+ req.headers['content-type']);
  console.log("HEADERS accept:"+ req.headers['accept']);
  console.log("HEADERS Authorization:"+ req.headers['Authorization']);
  
  console.log("BODY:"+ req.body);
  console.log("BODY RAW:"+ JSON.stringify(req.body));


  var uri = 'http://161.82.176.94';
  request({
      uri: uri + req.originalUrl,
      //headers: req.headers,
      method: req.method,
      headers: {
        'accept-encoding': req.headers['accept-encoding'],
        'content-type': req.headers['content-type'],
        'accept': req.headers['accept'],
        'device_type': req.headers['device_type'],
        'device_id': req.headers['device_id'],
        'app_id': req.headers['app_id'],
        'Authorization': req.headers['Authorization']
      },
      body: JSON.stringify(req.body)
      }).pipe(res);
      
});
